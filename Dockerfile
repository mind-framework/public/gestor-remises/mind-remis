FROM golang:alpine as api
ADD . /go/src/api
WORKDIR /go/src/api
RUN go mod vendor 
RUN go build
RUN go install


FROM node:alpine as ui
WORKDIR "/app"
ADD web /app
RUN apk add --no-cache python2 build-base gcc 
RUN yarn install
RUN yarn lint
RUN yarn build



FROM alpine
WORKDIR /app
COPY --from=api /go/bin/* /app/api
ADD db /app/db
COPY --from=ui /app/dist /app/web/dist
ENTRYPOINT [ "/app/api"]

