import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import installElementPlus from './plugins/element'
import installMind from '@mind-core-vue3'



const app = createApp(App)
installElementPlus(app)
installMind(app)
app.use(router).mount('#app')