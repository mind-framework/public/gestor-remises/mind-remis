import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '@/views/Home.vue'


const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: Home,
		meta: {
			view: {
				title: 'Inicio',
			},
			nav: {
				icon: 'el-icon-s-home',
			},
		}
  },
  {
		path: '/clientes',
		name: 'Clientes',
		component: () => import(/* webpackChunkName: "clientes" */ '../views/Clientes.vue'),
		meta: {
			view: {
				// title: 'Administración de Clientes',
			},
			nav: {
				icon: 'el-icon-s-custom',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "clientes" */ '../views/Cliente.vue'),
				meta: {
					view: {
						title: 'Datos del Cliente',
						titleNew: 'Alta de Cliente',
					},
				}
			}
		]
  },
  {
		path: '/moviles',
		name: 'Móviles',
		component: () => import(/* webpackChunkName: "moviles" */ '../views/Moviles.vue'),
		meta: {
			view: {
			},
			nav: {
				icon: 'el-icon-truck',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "moviles" */ '../views/Movil.vue'),
				meta: {
					view: {
						title: 'Detalles del Móvil',
						titleNew: 'Alta de Móvil',
					},
				}
			}
		]
  },
  {
		path: '/choferes',
		name: 'Choferes',
		component: () => import(/* webpackChunkName: "choferes" */ '../views/Choferes.vue'),
		meta: {
			view: {
				// title: 'Administración de Choferes',
			},
			nav: {
				icon: 'el-icon-user-solid',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "choferes" */ '../views/Chofer.vue'),
				meta: {
					view: {
						title: 'Detalles del Chofer',
						titleNew: 'Alta de Chofer',
					},
				}
			}
		]
  },
  {
		path: '/lugares',
		name: 'Lugares',
		component: () => import(/* webpackChunkName: "sitios" */ '../views/Lugares.vue'),
		meta: {
			view: {
			},
			nav: {
				icon: 'el-icon-location-information',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "sitios" */ '../views/Lugar.vue'),
				meta: {
					view: {
						title: 'Detalles de la locación',
						titleNew: 'Alta de Locación',
					},
				}
			}
		]
  },
  // {
	// 	path: '/planillas',
	// 	name: 'Planillas',
	// 	component: () => import(/* webpackChunkName: "planillas" */ '../views/Planillas.vue'),
	// 	meta: {
	// 		view: {
	// 			// title: 'Administración de Clientes',
	// 		},
	// 		nav: {
	// 			icon: 'el-icon-document',
	// 		},
	// 	},
	// 	children:[
	// 		{
	// 			path: ':id',
	// 			component: () => import(/* webpackChunkName: "planillas" */ '../views/Planilla.vue'),
	// 			meta: {
	// 				view: {
	// 					title: 'Detalle de Planilla',
	// 					titleNew: 'Nueva Planilla',
	// 				},
	// 			}
	// 		}
	// 	]
  // },
  {
		path: '/comisiones',
		name: 'Comisiones',
		component: () => import(/* webpackChunkName: "comisiones" */ '../views/Comisiones.vue'),
		meta: {
			view: {
				// title: 'Administración de Clientes',
			},
			nav: {
				icon: 'el-icon-setting',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "comisiones" */ '../views/Comision.vue'),
				meta: {
					view: {
						title: 'Detalle de Comisión',
						titleNew: 'Nueva Comisión',
					},
				}
			}
		]
	},
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
