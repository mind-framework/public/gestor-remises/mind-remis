import {Model} from '@mind-core-vue3'

class M extends Model {
	constructor() {
		super({
			uri:'api/agencias/1/planillas',
			key: 'id',
			cacheTimeout: 0,
			template: {
			}
		})
	}
}


export default new M()