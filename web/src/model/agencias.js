import {Model} from '@mind-core-vue3'

class M extends Model {
	constructor() {
		super({
			uri:'api/agencias',
			key: 'id',
			cacheTimeout: 1,
			template: {
			}
		})
	}
}


export default new M()