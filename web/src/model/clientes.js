import {Model} from '@mind-core-vue3'

class M extends Model {
	constructor() {
		super({
			uri:'api/agencias/1/clientes',
			key: 'id',
			cacheTimeout: 600,
			template: {
			}
		})
	}
}


export default new M()