import {Model} from '@mind-core-vue3'

class M extends Model {
	constructor() {
		super({
			uri:'api/agencias/1/moviles',
			key: 'movil',
			cacheTimeout: 1,
			template: {
			}
		})
	}
}


export default new M()