package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	// paquetes requeridos para mantejo de requests
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"net/http"

	//paquetes requeridos para la conexión a la BD
	_ "github.com/go-sql-driver/mysql"
	// "github.com/jmoiron/sqlx"

	//helpers para handlers y modelos
	_ "gitlab.com/mind-framework/core/mind-core-api/app"
	"gitlab.com/mind-framework/core/mind-core-api/handler"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/model"

	_log "github.com/rs/zerolog/log"

	//paquetes específicos del proyecto
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/agencias"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/choferes"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/clientes"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/comisiones"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/lugares"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/moviles"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/planillas"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/viajes"
)

var log = _log.With().Str("pkg", "main").Logger()

func main() {

	//obtengo los parámetros desde las variables de entorno
	apiPort := os.Getenv("API_PORT")
	if apiPort == "" {
		apiPort = "8080"
	}

	dbType := os.Getenv("DB_TYPE")
	if dbType == "" {
		dbType = "mysql"
	}

	connStr, err := getDBConnectionString()
	if err != nil {
		log.Fatal().Err(err).Str("cat", "env").Msg("Error al Cargar Datos de conexión a BD")
	}

	model.Init(dbType, connStr)

	//Configuración de Rutas
	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(middlewares.ParseParamsCtx)
	r.Use(middleware.Logger)
	// r.Get("/", func(w http.ResponseWriter, r *http.Request) {
	// })
	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`pong`))
	})

	//Ruta para acceder a los diferentes recursos de una agencia
	r.Route("/api/agencias/{id_agencia}", func(r chi.Router) {
		r.Use(agencias.FiltrarAgenciaMdw)
		r.Use(agencias.AgregarIDAgenciaMdw)
		r.Mount("/clientes", handler.NewCRUDHandler(clientes.NewModel).Routes())
		r.Mount("/moviles", handler.NewCRUDHandler(moviles.NewModel).Routes())
		r.Mount("/choferes", handler.NewCRUDHandler(choferes.NewModel).Routes())
		r.Mount("/viajes", handler.NewCRUDHandler(viajes.NewModel).Routes())
		r.Mount("/planillas", handler.NewCRUDHandler(planillas.NewModel).Routes())
		r.Mount("/comisiones", handler.NewCRUDHandler(comisiones.NewModel).Routes())
		r.Mount("/lugares", handler.NewCRUDHandler(lugares.NewModel).Routes())
	})

	//ruta para acceder a la información de Agencias
	//TODO: hacer funcionar metodos GET POST PATCH y DELETE sobre agencias
	r.Mount("/api/agencias", handler.NewCRUDHandler(agencias.NewModel).Routes())

	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "web/dist"))
	FileServer(r, "/", filesDir)

	log.Info().Str("puerto", apiPort).Msg("Iniciando Listener")

	if err := http.ListenAndServe(":"+apiPort, r); err != nil {
		log.Fatal().Err(err).Str("cat", "http").Msg("No se pudo iniciar servicio http")
	}

}

//getDBConnectionString devuelve el connection string utilizando los datos recibidos en las variables de entorno
func getDBConnectionString() (string, error) {
	log = log.With().Str("ctx", "getDBConnectionString").Logger()
	connStr := os.Getenv("DB_CONNECTION_STRING")
	if connStr == "" {
		dbHost := os.Getenv("DB_HOST")
		if dbHost == "" {
			return "", errors.New("DB_HOST no especificado")
		}
		log.Debug().Str("DB_HOST", dbHost).Msg("env")

		dbPort := os.Getenv("DB_PORT")
		if dbPort == "" {
			dbPort = "3306"
			log.Warn().Str("Puerto", dbPort).Msg("No se definió el puerto de conexión a BD, utilizando puerto por default")
		}
		log.Debug().Str("DB_PORT", dbPort).Msg("env")

		dbName := os.Getenv("API_DB_NAME")
		if dbName == "" {
			return "", errors.New("API_DB_NAME no especificado")
		}
		log.Debug().Str("API_DB_NAME", dbName).Msg("env")

		dbUserName := os.Getenv("API_DB_USERNAME")
		if dbUserName == "" {
			return "", errors.New("API_DB_USERNAME no especificado")
		}
		log.Debug().Str("API_DB_USERNAME", dbUserName).Msg("env")

		dbPassword := os.Getenv("API_DB_SECRET")
		if dbPassword == "" {
			return "", errors.New("API_DB_SECRET no especificado")
		}
		connStr = fmt.Sprintf("%s:%s@(%s:%s)/%s", dbUserName, dbPassword, dbHost, dbPort, dbName)
	}
	return connStr, nil
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}
