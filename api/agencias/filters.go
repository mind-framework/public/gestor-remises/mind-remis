package agencias

import (
	"gitlab.com/mind-framework/core/mind-core-api/model"
)


//validFilters es una mapa de Filtros disponibles para este modelo
// los filtros son utilizados en la consulta a la BD
var validFilters map[string]model.Filter


func init() {
	log.Debug().Msg("Inicializando Model")
	validFilters = make(map[string]model.Filter)

	validFilters["q"] = model.Filter{
		Description: "Buscar Agencia por Nombre o Dirección",
		ValidValues: []string{".*"},
		Parser: func(values []string) (string, []string, error){
			var valores []string
			valores = append(valores, "%" + values[0] + "%")
			valores = append(valores, "%" + values[0] + "%")
			return "nombre like ? or direccion_calle like ?", valores, nil
		},
	}

	validFilters["nombre"] = model.Filter{
		Description: "Filtra por Nombre de Agencia",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("nombre", true),
	}
}
