package agencias

import (
	"encoding/json"
	_log "github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
	"gitlab.com/mind-framework/public/gestor-remises/api-viajes/api/common"
)

var log = _log.With().Str("pkg", "agencias").Logger()

// recurso es la estructura de datos utilizada para almacenar los datos leídos de la BD
type recurso struct {
	ID 					string  `json:"id" db:"id"`
	Nombre			string	`json:"nombre"  db:"nombre"`
	Cuit				string  `json:"cuit" db:"cuit"`
	common.Locacion 
}

//NewModel devuelve una nueva estructura del tipo Modeler
func NewModel() model.Modeler {
	m := &Model{}
	m.Name = "agencias"
	m.PrimaryKeys = []string{"id"}
	m.Meta = make(map[string]interface{})

	return m
}

//Model ...
type Model struct {
	model.Model
}

//List obtiene la lista de clientes
func (m *Model) List(sort []string, limit string, offset string, filters map[string][]string) (model.Modeler, error) {

	m.Data = &[]recurso{}

	//parseo los filtros recibidos 
	wQSL, values, err := model.ParseFiters(filters, validFilters)
	if err != nil {
		return nil, err		
	}
	
	log.Debug().Interface("sql", wQSL).Interface("binds", values).Msg("List")
		if err := model.Select("*").Into(m).From(m.Name).Limit(limit).OrderBy(sort).Offset(offset).Where(wQSL...).Exec(values...); err != nil {
			return nil, err
		}

	m.Meta["filters"] = validFilters
	return m, nil
}

//Get Obtine el recurso solicitado
func (m *Model)  Get(id ...interface{}) (model.Modeler, error)  {
	m.Data = &recurso{}

	if err := model.Get(m, id); err != nil {
		return nil, err
	}

	return m, nil
}

//Delete elimina el recurso Especificado
func (m *Model) Delete(id ...interface{}) (error)  {

	if err := model.Delete(m, id); err != nil {
		return err
	}
	return nil
}

//Insert Agrega un registro en el storage
func (m *Model)  Insert(data interface{}) (model.Modeler, error)  {
	var err error

	//recursos es una lista de recursos.
	// en esta lista se van a colocar los datos recibidos en data
	recursos := &[]recurso{}

	byteData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &recursos)
	if err != nil {
		return nil, err
	}

	//recorro todos los registros recibidos para insertar
	for i := range *recursos {
		log.Debug().Interface("i", i).Msg("")
		c := (*recursos)[i]
		lastID, err := model.Insert().Into(m.Name).
			Columns(
				"nombre", 
				"cuit",
				"calle", 
				"numero",
				"localidad",
				"partido",
				"provincia",
			).
			Values(
				c.Nombre, 
				c.Cuit,
				c.Calle, 
				c.Numero,
				c.Localidad,
				c.Partido,
				c.Provincia,
			)
		if err != nil {
			return nil, err
		}
	
		r, err := m.Get(lastID) 
		if err != nil {
			return nil, err
		}
		var data *recurso
		data = r.GetData().(*recurso)
		(*recursos)[i] = *data
	}

	//guardo los datos actualizados en m.Data
	m.Data = recursos

	
	return m, err
}

//Update actualiza un recurso
func (m *Model) Update(data interface{}, ids ...interface{}) (model.Modeler, error) {
	var err error
	c := data.(*recurso)

	err = model.Update(m.Name).Set(
				"nombre", 
				"cuit",
				"calle", 
				"numero",
				"localidad",
				"partido",
				"provincia",
	).
	Values(
				c.Nombre, 
				c.Cuit,
				c.Calle, 
				c.Numero,
				c.Localidad,
				c.Partido,
				c.Provincia,
	).Where(m.GetPrimaryKeys(), ids)
	return m, err

}