package moviles

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
	// "api-viajes/api/common"
)


// recurso es la estructura de datos utilizada para almacenar los datos leídos de la BD
type recurso struct {
	// ID 					string  `json:"id" db:"id"`
	IDAgencia  	*int `json:"idAgencia"  db:"id_agencia"`
	Movil				int	`json:"movil"  db:"movil"`
	Patente			string	`json:"patente"  db:"patente"`
	Marca				string	`json:"marca"  db:"marca"`
	Modelo			*string	`json:"modelo"  db:"modelo"`
	Anio				int	`json:"año"  db:"anio"`
	Color				string	`json:"color"  db:"color"`
	Detalles		*string	`json:"detalles"  db:"detalles"`
	Activo			bool	`json:"activo"  db:"activo"`
	IDComision	*int 	`json:"idComision"  db:"id_comision"`
}




//NewModel devuelve una nueva estructura del tipo Modeler
func NewModel() model.Modeler {
	m := &Model{}
	m.Name = "moviles"
	m.PrimaryKeys = []string{"movil", "id_agencia"}
	m.Meta = make(map[string]interface{})

	return m
}

//Model ...
type Model struct {
	model.Model
}


//List obtiene la lista de clientes
func (m *Model) List(sort []string, limit string, offset string, filters map[string][]string) (model.Modeler, error) {

	m.Data = &[]recurso{}

	//parseo los filtros recibidos 
	wQSL, values, err:= model.ParseFiters(filters, validFilters)
	if err != nil {
		return nil, err		
	}
	log.Debug().Interface("sql", wQSL).Interface("binds", values).Msg("List")

	if err := model.Select("*").Into(m).From(m.Name).Limit(limit).OrderBy(sort).Offset(offset).Where(wQSL...).Exec(values...); err != nil {
		return nil, err
	}

	m.Meta["filters"] = validFilters
	return m, nil
}

//Get Obtine el recurso solicitado
func (m *Model)  Get(id ...interface{}) (model.Modeler, error)  {
	m.Data = &recurso{}

	if err := model.Get(m, id...); err != nil {
		return nil, err
	}

	return m, nil
}

//Delete elimina el recurso Especificado
func (m *Model) Delete(id ...interface{}) (error)  {

	if err := model.Delete(m, id...); err != nil {
		return err
	}
	return nil
}

//Insert Agrega un registro en el storage
func (m *Model)  Insert(data interface{}) (model.Modeler, error)  {
	var err error

	//recursos contiene los datos recibidos en el request
	recursos := &[]recurso{}

	byteData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &recursos)
	if err != nil {
		return nil, err
	}

	//recorro todos los registros recibidos para insertar
	for i := range *recursos {
		log.Debug().Interface("i", i).Msg("")
		c := (*recursos)[i]
		_, err := model.Insert().Into(m.Name).
			Columns(
				"id_agencia",
				"movil",
				"marca",
				"modelo",
				"anio",
				"patente",
				"color",
				"detalles",
				"activo",
				"id_comision",
			).
			Values(
				c.IDAgencia,
				c.Movil,
				c.Marca,
				c.Modelo,
				c.Anio,
				c.Patente,
				c.Color,
				c.Detalles,
				c.Activo,
				c.IDComision,
			)
		if err != nil {
			return nil, err
		}
	
		//Obtengo los datos del registro insertado para devolverlo como resultado
		r, err := m.Get(c.Movil, c.IDAgencia) 
		if err != nil {
			return nil, err
		}
		var data *recurso
		data = r.GetData().(*recurso)
		(*recursos)[i] = *data
	}

	//guardo los datos actualizados en m.Data
	m.Data = recursos

	
	return m, err
}

//Update actualiza un recurso
func (m *Model) Update(data interface{}, ids ...interface{}) (model.Modeler, error) {
	var err error
	c := data.(*recurso)

	err = model.Update(m.Name).Set(
				"id_agencia",
				"movil",
				"marca",
				"modelo",
				"anio",
				"patente",
				"color",
				"detalles",
				"activo",
				"id_comision",
	).
	Values(
				c.IDAgencia,
				c.Movil,
				c.Marca,
				c.Modelo,
				c.Anio,
				c.Patente,
				c.Color,
				c.Detalles,
				c.Activo,
				c.IDComision,
	).Where(m.GetPrimaryKeys(), ids)
	return m, err

}