package viajes

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
	"errors"
	// "api-viajes/api/common"
)


// recurso es la estructura de datos utilizada para almacenar los datos leídos de la BD
type recurso struct {
	IDAgencia  	*uint64 `json:"idAgencia"  db:"id_agencia"`
	ID 					uint64  `json:"id" db:"id"`
	IDCliente  	*uint64 `json:"idCliente"  db:"id_cliente"`
	TS					*string  `json:"timestamp" db:"ts"`
	TipoOrigen				*string	`json:"tipoOrigen"  db:"tipo_origen"`
	OCalle				*string	`json:"origenCalle"  db:"o_calle"`
	ONumero				*string	`json:"origenNumero"  db:"o_numero"`
	OLocalidad		*string	`json:"origenLocalidad"  db:"o_localidad"`
	OCalle1				*string	`json:"origenCalle1"  db:"o_calle1"`
	OCalle2				*string	`json:"origenCalle2"  db:"o_calle2"`
	TipoDestino				*string	`json:"tipoDestino"  db:"tipo_destino"`
	DCalle				*string	`json:"destinoCalle"  db:"d_calle"`
	DNumero				*string	`json:"destinoNumero"  db:"d_numero"`
	DLocalidad		*string	`json:"destinoLocalidad"  db:"d_localidad"`
	Fecha		*string	`json:"fecha"  db:"fecha"`
	Hora		*string	`json:"hora"  db:"hora"`
	HoraSalida		*string	`json:"horaSalida"  db:"hora_salida"`
	FechaLlegada		*string	`json:"fechaLlegada"  db:"fecha_llegada"`
	HoraLlegada		*string	`json:"horaLlegada"  db:"hora_llegada"`
	Estado		*string	`json:"estado"  db:"estado"`
	Km		*string	`json:"km"  db:"km"`
	IDPlanilla		*uint64	`json:"idPlanilla"  db:"planilla"`
	Importe		*float64	`json:"importe"  db:"importe"`
	Espera		*float64	`json:"espera"  db:"espera"`
	Obs		*string	`json:"obs"  db:"obs"`
	AnticipacionReserva		*uint64	`json:"anticipacionReserva"  db:"anticipacion_reserva"`
}




//NewModel devuelve una nueva estructura del tipo Modeler
func NewModel() model.Modeler {
	m := &Model{}
	m.Name = "viajes"
	m.PrimaryKeys = []string{"id", "id_agencia"}
	m.Meta = make(map[string]interface{})

	return m
}

//Model ...
type Model struct {
	model.Model
}


//List obtiene la lista de clientes
func (m *Model) List(sort []string, limit string, offset string, filters map[string][]string) (model.Modeler, error) {

	m.Data = &[]recurso{}

	//parseo los filtros recibidos 
	wQSL, values, err := model.ParseFiters(filters, validFilters)
	if err != nil {
		return nil, err		
	}

	log.Debug().Interface("sql", wQSL).Interface("binds", values).Msg("List")

	if err := model.Select("*").Into(m).From(m.Name).Limit(limit).OrderBy(sort).Offset(offset).Where(wQSL...).Exec(values...); err != nil {
		return nil, err
	}

	m.Meta["filters"] = validFilters
	return m, nil
}

//Get Obtine el recurso solicitado
func (m *Model)  Get(id ...interface{}) (model.Modeler, error)  {
	m.Data = &recurso{}

	if err := model.Get(m, id...); err != nil {
		return nil, err
	}

	return m, nil
}

//Delete elimina el recurso Especificado
func (m *Model) Delete(id ...interface{}) (error)  {
	m.Data = &recurso{}

	if err := model.Get(m, id...); err != nil {
		return  err
	}
	c := m.Data.(*recurso)
	estado := "Anulado"
	c.Estado = &estado
	log.Debug().Interface("ID", id).Msg("Anulando Viaje")
	if _, err := m.Update(c, id...); err != nil {
		return err		
	}
	// if err := model.Delete(m, id...); err != nil {
	// 	return err
	// }
	return nil
}

//Insert Agrega un registro en el storage
func (m *Model)  Insert(data interface{}) (model.Modeler, error)  {
	var err error

	//recursos contiene los datos recibidos en el request
	recursos := &[]recurso{}

	byteData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &recursos)
	if err != nil {
		return nil, err
	}

	//recorro todos los registros recibidos para insertar
	for i := range *recursos {
		log.Debug().Interface("i", i).Msg("")
		c := (*recursos)[i]

		if c.Estado == nil {
			estado := "Pendiente"
			c.Estado = &estado
		}

		lastID, err := model.Insert().Into(m.Name).
			Columns(
				"id_agencia",
				"o_calle",
				"o_numero",
				"o_localidad",
				"o_calle1",
				"o_calle2",
				"d_calle",
				"d_numero",
				"d_localidad",
				"fecha",
				"hora",
				"hora_salida",
				"fecha_llegada",
				"hora_llegada",
				"estado",
				"km",
				"planilla",
				"importe",
				"espera",
				"obs",
				"tipo_origen",
				"tipo_destino",
				"anticipacion_reserva",
				"id_cliente",
			).
			Values(
				c.IDAgencia,
				c.OCalle,
				c.ONumero,
				c.OLocalidad,
				c.OCalle1,
				c.OCalle2,
				c.DCalle,
				c.DNumero,
				c.DLocalidad,
				c.Fecha,
				c.Hora,
				c.HoraSalida,
				c.FechaLlegada,
				c.HoraLlegada,
				c.Estado,
				c.Km,
				c.IDPlanilla,
				c.Importe,
				c.Espera,
				c.Obs,
				c.TipoOrigen,
				c.TipoDestino,
				c.AnticipacionReserva,
				c.IDCliente,
			)


		if err != nil {
			return nil, err
		}
	
		//Obtengo los datos del registro insertado para devolverlo como resultado
		r, err := m.Get(lastID, c.IDAgencia) 
		if err != nil {
			return nil, err
		}
		var data *recurso
		data = r.GetData().(*recurso)
		(*recursos)[i] = *data
	}

	//guardo los datos actualizados en m.Data
	m.Data = recursos

	
	return m, err
}

//Update actualiza un recurso
func (m *Model) Update(data interface{}, ids ...interface{}) (model.Modeler, error) {
	var err error
	c := data.(*recurso)

	if *c.Estado == "En Curso" && c.IDPlanilla == nil {
		return nil, errors.New("No se puede despachar un movil sin asignarle una planilla")
	}
	if *c.Estado == "Finalizado" && c.IDPlanilla == nil {
		return nil, errors.New("Error al finalizar viaje: id de planilla no definido")
	}


	err = model.Update(m.Name).Set(
		"id_agencia",
		"o_calle",
		"o_numero",
		"o_localidad",
		"o_calle1",
		"o_calle2",
		"d_calle",
		"d_numero",
		"d_localidad",
		"fecha",
		"hora",
		"hora_salida",
		"fecha_llegada",
		"hora_llegada",
		"estado",
		"km",
		"planilla",
		"importe",
		"espera",
		"obs",
		"tipo_origen",
		"tipo_destino",
		"anticipacion_reserva",
		"id_cliente",
	).
	Values(
		c.IDAgencia,
		c.OCalle,
		c.ONumero,
		c.OLocalidad,
		c.OCalle1,
		c.OCalle2,
		c.DCalle,
		c.DNumero,
		c.DLocalidad,
		c.Fecha,
		c.Hora,
		c.HoraSalida,
		c.FechaLlegada,
		c.HoraLlegada,
		c.Estado,
		c.Km,
		c.IDPlanilla,
		c.Importe,
		c.Espera,
		c.Obs,
		c.TipoOrigen,
		c.TipoDestino,
		c.AnticipacionReserva,
		c.IDCliente,
	).Where(m.GetPrimaryKeys(), ids)
	return m, err

}