package comisiones

import (
	// "encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
)


//validFilters es una mapa de Filtros disponibles para este modelo
// los filtros son utilizados en la consulta a la BD
var validFilters map[string]model.Filter


func init() {
	log.Debug().Msg("Inicializando Model")
	validFilters = make(map[string]model.Filter)

	validFilters["idAgencia"] = model.Filter{
		Description: "Filtra por ID de Agencia",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("id_agencia", false),
	}

	
	validFilters["nombre"] = model.Filter{
		Description: "Filtra por Nombre",
		ValidValues: []string{".*"},
		Parser: model.NewGenericFilterParser("nombre", true),
	}



}
