package planillas


import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/mind-core-api/model"
	// "api-viajes/api/common"
)


// recurso es la estructura de datos utilizada para almacenar los datos leídos de la BD
type recurso struct {
	IDAgencia  	*int `json:"idAgencia"  db:"id_agencia"`
	ID 					int  `json:"id" db:"id"`
	TSUpdate					*string  `json:"tsUpdate" db:"ts_update"`
	TSAnterior					*string  `json:"tsAnterior" db:"ts_anterior"`
	Movil  	*int `json:"movil"  db:"movil"`
	IDChofer  	*int `json:"idChofer"  db:"id_chofer"`
	Turno		*string	`json:"turno"  db:"turno"`
	FechaApertura				*string	`json:"fechaApertura"  db:"fecha_apertura"`
	HoraApertura				*string	`json:"horaApertura"  db:"hora_apertura"`
	FechaCierre				*string	`json:"fechaCierre"  db:"fecha_cierre"`
	HoraCierre				*string	`json:"horaCierre"  db:"hora_cierre"`
	Estado		*string	`json:"estado"  db:"estado"`
	EstadoMovil		*string	`json:"estadoMovil"  db:"estado_movil"`
	ComisionImporte  	*float64 `json:"comisionImporte"  db:"comision_importe"`
	ComisionPorcentaje  	*int `json:"comisionPorcentaje"  db:"comision_porcentaje"`
	TotalPlanilla  	*float64 `json:"totalPlanilla"  db:"total_planilla"`
	Gastos  	*float64 `json:"gastos"  db:"gastos"`
	Combustible  	*float64 `json:"combustible"  db:"combustible"`
	GastosAgencia  	*float64 `json:"gastosAgencia"  db:"gastos_agencia"`
	CombustibleAgencia  	*float64 `json:"combustibleAgencia"  db:"combustible_agencia"`
	Viajes *int `json:"viajes" db:"viajes"`
	ViajesActivos *int `json:"viajesActivos" db:"viajes_activos"`
	Obs		*string	`json:"obs"  db:"obs"`
}




//NewModel devuelve una nueva estructura del tipo Modeler
func NewModel() model.Modeler {
	m := &Model{}
	m.Name = "planillas"
	m.PrimaryKeys = []string{"id", "id_agencia"}
	m.Meta = make(map[string]interface{})

	return m
}

//Model ...
type Model struct {
	model.Model
}


//List obtiene la lista de clientes
func (m *Model) List(sort []string, limit string, offset string, filters map[string][]string) (model.Modeler, error) {

	m.Data = &[]recurso{}

	//parseo los filtros recibidos 
	wQSL, values, err := model.ParseFiters(filters, validFilters)
	if err != nil {
		return nil, err		
	}

	log.Debug().Interface("sql", wQSL).Interface("binds", values).Msg("List")

	if err := model.Select("planillas.*,count(viajes.id) as viajes,sum(if(viajes.estado = 'En Curso',1,0)) as viajes_activos").Into(m).From(m.Name).
		LeftJoin("viajes").On("planillas.id = viajes.planilla").
		Where(wQSL...).
		GroupBy("planillas.id").
		Limit(limit).OrderBy(sort).Offset(offset).
		Exec(values...); err != nil {
			return nil, err
	}

	//Establezco el estado del movil dependiendo de la cantidad de viajes que tiene activos
	for _,v := range *m.Data.(*[]recurso) {
		log.Trace().Interface("Planilla", v).Msg("")
		if *v.ViajesActivos > 0 {
			*v.EstadoMovil = "Ocupado"
		}
	}

	m.Meta["filters"] = validFilters
	return m, nil
}

//Get Obtine el recurso solicitado
func (m *Model)  Get(id ...interface{}) (model.Modeler, error)  {
	m.Data = &recurso{}

	if err := model.Get(m, id...); err != nil {
		return nil, err
	}

	return m, nil
}

//Delete elimina el recurso Especificado
func (m *Model) Delete(id ...interface{}) (error)  {
	m.Data = &recurso{}

	if err := model.Get(m, id...); err != nil {
		return  err
	}
	c := m.Data.(*recurso)
	estado := "Anulada"
	c.Estado = &estado
	log.Debug().Interface("ID", id).Msg("Anulando Planilla")
	if _, err := m.Update(c, id...); err != nil {
		return err		
	}
	return nil
}

//Insert Agrega un registro en el storage
func (m *Model)  Insert(data interface{}) (model.Modeler, error)  {
	var err error

	//recursos contiene los datos recibidos en el request
	recursos := &[]recurso{}

	byteData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(byteData, &recursos)
	if err != nil {
		return nil, err
	}

	//recorro todos los registros recibidos para insertar
	for i := range *recursos {
		log.Debug().Interface("i", i).Msg("")
		c := (*recursos)[i]

		if c.Estado == nil {
			estado := "Pendiente"
			c.Estado = &estado
		}

		lastID, err := model.Insert().Into(m.Name).
			Columns(
				"id_agencia",
				"movil",
				"id_chofer",
				"fecha_apertura",
				"hora_apertura",
				"fecha_cierre",
				"hora_cierre",
				"estado",
				"estado_movil",
				"comision_importe",
				"comision_Porcentaje",
				"total_planilla",
				"obs",
				"gastos",
				"combustible",
				"gastos_agencia",
				"combustible_agencia",
				"turno",
			).
			Values(
				c.IDAgencia,
				c.Movil,
				c.IDChofer,
				c.FechaApertura,
				c.HoraApertura,
				c.FechaCierre,
				c.HoraCierre,
				c.Estado,
				c.EstadoMovil,
				c.ComisionImporte,
				c.ComisionPorcentaje,
				c.TotalPlanilla,
				c.Obs,
				c.Gastos,
				c.Combustible,
				c.GastosAgencia,
				c.CombustibleAgencia,
				c.Turno,
			)


		if err != nil {
			return nil, err
		}
	
		//Obtengo los datos del registro insertado para devolverlo como resultado
		r, err := m.Get(lastID, c.IDAgencia) 
		if err != nil {
			return nil, err
		}
		var data *recurso
		data = r.GetData().(*recurso)
		(*recursos)[i] = *data
	}

	//guardo los datos actualizados en m.Data
	m.Data = recursos

	
	return m, err
}

//Update actualiza un recurso
func (m *Model) Update(data interface{}, ids ...interface{}) (model.Modeler, error) {
	var err error
	c := data.(*recurso)

	err = model.Update(m.Name).Set(
				"id_agencia",
				"movil",
				"fecha_apertura",
				"hora_apertura",
				"fecha_cierre",
				"hora_cierre",
				"estado",
				"estado_movil",
				"comision_importe",
				"comision_Porcentaje",
				"total_planilla",
				"obs",
				"gastos",
				"combustible",
				"gastos_agencia",
				"combustible_agencia",
				"turno",
	).
	Values(
				c.IDAgencia,
				c.Movil,
				c.FechaApertura,
				c.HoraApertura,
				c.FechaCierre,
				c.HoraCierre,
				c.Estado,
				c.EstadoMovil,
				c.ComisionImporte,
				c.ComisionPorcentaje,
				c.TotalPlanilla,
				c.Obs,
				c.Gastos,
				c.Combustible,
				c.GastosAgencia,
				c.CombustibleAgencia,
				c.Turno,
	).Where(m.GetPrimaryKeys(), ids)
	return m, err

}