DROP TRIGGER IF EXISTS `viajes_AFTER_INSERT`;

CREATE TRIGGER `viajes_AFTER_INSERT` AFTER INSERT ON `viajes` FOR EACH ROW BEGIN
	INSERT IGNORE INTO lugares 
		(id_agencia, tipo, nombre, localidad)
	values (NEW.id_agencia,NEW.tipo_origen, NEW.o_calle ,NEW.o_localidad);
	INSERT IGNORE INTO lugares 
		(id_agencia, tipo, nombre, localidad)
	values (NEW.id_agencia, NEW.tipo_destino, NEW.d_calle ,NEW.d_localidad);
END;
