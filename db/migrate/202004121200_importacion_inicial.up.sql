/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agencias`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `agencias` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calle` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` int DEFAULT NULL,
  `provincia` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partido` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `localidad` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `choferes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `choferes` (
  `id_agencia` int unsigned NOT NULL,
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `dni` varchar(10) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_egreso` date DEFAULT NULL,
  `vencimiento_registro` date DEFAULT NULL,
  `activo` int DEFAULT '1',
  `movil` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`id_agencia`),
  KEY `fk_choferes_1_idx` (`id_agencia`),
  CONSTRAINT `fk_choferes_1` FOREIGN KEY (`id_agencia`) REFERENCES `agencias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clientes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_agencia` int unsigned NOT NULL DEFAULT '0',
  `nombre` varchar(100) DEFAULT NULL,
  `calle` varchar(100) DEFAULT NULL,
  `numero` int unsigned DEFAULT NULL,
  `piso` int unsigned DEFAULT NULL,
  `depto` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `movil` varchar(45) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `localidad` varchar(45) DEFAULT NULL,
  `partido` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `obs` varchar(1000) DEFAULT NULL,
  `calle1` varchar(45) DEFAULT NULL,
  `calle2` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clientes_1_idx` (`id_agencia`),
  CONSTRAINT `fk_clientes_1` FOREIGN KEY (`id_agencia`) REFERENCES `agencias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comisiones`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `comisiones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_agencia` int unsigned NOT NULL,
  `nombre` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `comision` int DEFAULT '0',
  `combustible_agencia` int DEFAULT '0',
  `gastos_agencia` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_comisiones_1_idx` (`id_agencia`),
  CONSTRAINT `fk_comisiones_1` FOREIGN KEY (`id_agencia`) REFERENCES `agencias` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `localidades`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `localidades` (
  `provincia` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partido` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `localidad` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`partido`,`provincia`,`localidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lugares`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `lugares` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_agencia` int unsigned NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `localidad` varchar(45) NOT NULL,
  `tipo` varchar(10) DEFAULT 'Calle',
  `prioridad` tinyint DEFAULT '5',
  `nombre_alternativo` varchar(50) DEFAULT NULL,
  `frecuente` int unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index2` (`id_agencia`,`nombre`,`localidad`)
) ENGINE=InnoDB AUTO_INCREMENT=1622 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moviles`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `moviles` (
  `id_agencia` int unsigned NOT NULL DEFAULT '0',
  `movil` int unsigned NOT NULL,
  `patente` varchar(10) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `anio` smallint unsigned DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `detalles` varchar(1000) DEFAULT NULL,
  `activo` int DEFAULT '1',
  `id_comision` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id_agencia`,`movil`),
  UNIQUE KEY `patente_UNIQUE` (`patente`),
  CONSTRAINT `fk_moviles_1` FOREIGN KEY (`id_agencia`) REFERENCES `agencias` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `planillas`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `planillas` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_agencia` int unsigned NOT NULL,
  `ts_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `movil` int unsigned NOT NULL,
  `id_chofer` int unsigned DEFAULT NULL,
  `fecha_apertura` date NOT NULL,
  `hora_apertura` time NOT NULL,
  `turno` enum('Día','Noche','Intermedio') NOT NULL,
  `fecha_cierre` date DEFAULT NULL,
  `hora_cierre` time DEFAULT NULL,
  `estado` enum('Abierta','Cerrada','Anulada','Suspendida') DEFAULT 'Abierta',
  `obs` varchar(1000) DEFAULT NULL,
  `estado_movil` enum('Libre','Ocupado') DEFAULT 'Libre',
  `ts_anterior` timestamp NULL DEFAULT NULL,
  `comision_importe` decimal(10,2) unsigned DEFAULT '0.00',
  `comision_porcentaje` int unsigned DEFAULT '20',
  `total_planilla` decimal(10,2) unsigned DEFAULT '0.00',
  `gastos` decimal(10,2) DEFAULT '0.00',
  `combustible` decimal(10,2) DEFAULT '0.00',
  `gastos_agencia` decimal(10,2) DEFAULT '0.00',
  `combustible_agencia` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Índice 4` (`fecha_apertura`,`movil`,`turno`),
  KEY `Índice 2` (`fecha_apertura`,`hora_apertura`,`hora_cierre`),
  KEY `Índice 3` (`movil`,`fecha_apertura`),
  KEY `fk_planillas_1_idx` (`id_agencia`),
  KEY `fk_planillas_3_idx` (`id_chofer`),
  KEY `fk_planillas_2_idx` (`id_agencia`,`movil`),
  CONSTRAINT `fk_planillas_1` FOREIGN KEY (`id_agencia`) REFERENCES `agencias` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_planillas_2` FOREIGN KEY (`id_agencia`, `movil`) REFERENCES `moviles` (`id_agencia`, `movil`),
  CONSTRAINT `fk_planillas_3` FOREIGN KEY (`id_chofer`) REFERENCES `choferes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `viajes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `viajes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_agencia` int unsigned NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_cliente` int unsigned DEFAULT NULL,
  `o_calle` varchar(45) DEFAULT '',
  `o_numero` varchar(45) DEFAULT '',
  `o_localidad` varchar(45) DEFAULT '',
  `o_calle1` varchar(45) DEFAULT NULL,
  `o_calle2` varchar(45) DEFAULT NULL,
  `d_calle` varchar(45) DEFAULT '',
  `d_numero` varchar(45) DEFAULT '',
  `d_localidad` varchar(45) DEFAULT '',
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `hora_salida` time DEFAULT NULL,
  `fecha_llegada` date DEFAULT NULL,
  `hora_llegada` time DEFAULT NULL,
  `estado` enum('Pendiente','Reserva','Asignado','Despachado','En Curso','Finalizado','Anulado') NOT NULL DEFAULT 'Pendiente',
  `km` smallint unsigned DEFAULT '0',
  `planilla` int unsigned DEFAULT NULL,
  `importe` decimal(7,2) unsigned DEFAULT '0.00',
  `espera` decimal(7,2) unsigned DEFAULT '0.00',
  `obs` varchar(255) DEFAULT NULL,
  `tipo_origen` enum('Calle','Lugar') DEFAULT 'Calle',
  `tipo_destino` enum('Calle','Lugar') DEFAULT 'Calle',
  `anticipacion_reserva` int unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index2` (`o_calle`,`o_numero`),
  KEY `index3` (`o_localidad`),
  KEY `index4` (`d_calle`,`d_numero`),
  KEY `index5` (`d_localidad`),
  KEY `movil` (`fecha`,`planilla`),
  KEY `fk_viajes_1_idx` (`id_agencia`),
  CONSTRAINT `fk_viajes_1` FOREIGN KEY (`id_agencia`) REFERENCES `agencias` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=831 DEFAULT CHARSET=utf8;
