module gitlab.com/mind-framework/public/gestor-remises/api-viajes

go 1.18

replace gitlab.com/mind-framework/core/mind-core-api => ./modules/mind-core-api

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/render v1.0.1
	github.com/go-sql-driver/mysql v1.4.0
	github.com/rs/zerolog v1.20.0
	gitlab.com/mind-framework/core/mind-core-api v0.1.5
)

require (
	github.com/containerd/containerd v1.6.2 // indirect
	github.com/docker/distribution v2.8.1+incompatible // indirect
	github.com/docker/docker v20.10.14+incompatible // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible // indirect
	github.com/jmoiron/sqlx v1.2.0 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/grpc v1.45.0 // indirect
)
